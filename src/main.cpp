/**
 *******************************************************************************
 * @file    Main.cpp
 * @author  Abu Bakar
 * @brief   This file contains the Main Application Stack for the Execture of 
 *          whole Firmware.
 *******************************************************************************
 */

#include "Config.h"
#include "Sensors.h"
#include "Debugger.h"

void setup(){
  initializeDebugger();
  #ifdef SENSOR_ENABLE
    #ifdef SENSOR_ENABLE_MPU6050
      initializeMPU6050();
    #endif
    #ifdef SENSOR_ENABLE_BME280
      initializebme280();
      
    #endif
    #ifdef SENSOR_ENABLE_DHT21
      initializeDHT21();
    #endif
  #endif
}

void loop(){
  #ifdef SENSOR_ENABLE
    #ifdef SENSOR_ENABLE_MPU6050
      updateMPU6050Data();
    #endif
    #ifdef SENSOR_ENABLE_BME280
      updateBME280Data();
    #endif
    #ifdef SENSOR_ENABLE_DHT21
      updateDHT21Data();
    #endif
  #endif
}
