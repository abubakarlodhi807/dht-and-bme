/**
 *******************************************************************************
 * @file    Config.h
 * @author  Abu Bakar
 * @brief   This file contains the configuration parameters, modules control,
 *          constant, definitions, pin definitions etc. for all the modules of 
 *          this codebase.
 *******************************************************************************
 */
#ifndef __CONFIG_H
#define __CONFIG_H

#include <Arduino.h>
#include "Sensors.h"

/******************************************************************************/
/*********************************** Device ***********************************/
/***************************** Config Parameters ******************************/
#define FW_VER                              "1.0.0"                             
/******************************************************************************/

/******************************************************************************/
/*********************************** Debugs ***********************************/
/***************************** Config Parameters ******************************/
#define DEBUG_PORT                          Serial
#define DEBUG_BAUD                          9600
/******************************************************************************/
/******************************************************************************/
/***************************** Debugs Components ******************************/
#define DEBUG_ENABLE_GLOBAL
#define DEBUG_ENABLE_MPU6050
#define DEBUG_ENABLE_BME280
#define DEBUG_ENABLE_DHT21
/******************************************************************************/
/***************************** Verbose Components *****************************/
#define VERBOSE_ENABLE_MPU6050
#define VERBOSE_ENABLE_BME280
#define VERBOSE_ENABLE_DHT21
/******************************************************************************/

/******************************************************************************/
/*********************************** Sensors **********************************/
/***************************** Config Parameters ******************************/
#define SENSOR_ENABLE
#define SENSOR_ENABLE_MPU6050
#define SENSOR_ENABLE_MPU6050_ACCEL
#define SENSOR_ENABLE_MPU6050_GYRO
#define SENSOR_ENABLE_MPU6050_TEMP
#define SENSOR_ENABLE_BME280
#define SENSOR_ENABLE_BME280_TEMP
#define SENSOR_ENABLE_DHT21
#define SENSOR_ENABLE_BME280_TEMP
/******************************************************************************/
/****************************** Update Intervals ******************************/
#define UPDATE_INTERVAL_MPU6050             2000
#define UPDATE_INTERVAL_BME280              2000
#define UPDATE_INTERVAL_BME280              2000
/******************************************************************************/

#endif
