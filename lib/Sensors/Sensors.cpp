#include "Sensors.h"

#ifdef SENSOR_ENABLE_MPU6050
  #include <MPU6050.h>
#endif
#ifdef SENSOR_ENABLE_BME280
  #include <Adafruit_Sensor.h>
  #include <Adafruit_BME280.h>
#endif
#ifdef SENSOR_ENABLE_DHT21
  #include <DHT.h>
#endif

#ifdef SENSOR_ENABLE
  #ifdef SENSOR_ENABLE_MPU6050
    MPU6050 mpu;  
    unsigned long previousUpdateMillisMPU6050 = 0;
    float temperatureMPU6050 = 0;
    int rawXAccelMPU6050 = 0;
    bool operationFlagMPU6050 = false;
  #endif
  #ifdef SENSOR_ENABLE_BME280
    Adafruit_BME280 bme;
    unsigned long previousUpdateMillisBME280 = 0;
    float temperatureBME280 = 0;
    float humidityBME280 = 0;
    float pressureBME280 = 0;
    float altitudeBME280 = 0;
    #define SEALEVELPRESSURE_HPA (1013.25)
  #endif
  #ifdef SENSOR_ENABLE_DHT21
    DHT dht(DHTPIN, DHTTYPE);
    float temperatureDHT21 = 0;
    float humidityDHT21 = 0;
  #endif  
#endif

#ifdef SENSOR_ENABLE 
  #ifdef SENSOR_ENABLE_MPU6050
    bool initializeMPU6050() {
      #ifdef DEBUG_ENABLE_GLOBAL
        #ifdef DEBUG_ENABLE_MPU6050
          DEBUG_PORT.println("Sensors: MPU6050: Intitializing...");
        #endif  
      #endif
      if(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G)) {
        #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_MPU6050
            DEBUG_PORT.println("Sensors: MPU6050: Intialization Failure!");
          #endif
        #endif
        return false;
      }
      mpu.calibrateGyro();
      mpu.setThreshold(3);
      #ifdef DEBUG_ENABLE_GLOBAL
        #ifdef DEBUG_ENABLE_MPU6050
          DEBUG_PORT.println("Sensors: MPU6050: Intialized...");
        #endif
      #endif
      operationFlagMPU6050 = true;
      return true; 
    }
    
    void updateMPU6050Data() {
      if(operationFlagMPU6050) {
        if (millis() - previousUpdateMillisMPU6050 > UPDATE_INTERVAL_MPU6050) {
          #ifdef SENSOR_ENABLE_MPU6050_GYRO
            #ifdef DEBUG_ENABLE_GLOBAL
              #ifdef DEBUG_ENABLE_MPU6050
                DEBUG_PORT.println("Sensors: MPU6050: Updating MPU6050 data...");
              #endif
            #endif
            Vector rawGyro = mpu.readRawGyro();
            Vector normGyro = mpu.readNormalizeGyro();
            #ifdef DEBUG_ENABLE_GLOBAL
              #ifdef DEBUG_ENABLE_MPU6050
                DEBUG_PORT.println("Sensors: MPU6050: Updating Gyro data...");
                rawXAccelMPU6050 = rawGyro.XAxis;
                DEBUG_PORT.println("Sensors: MPU6050: Raw: " + String(rawGyro.XAxis) + ", " + String(rawGyro.YAxis) + ", " + String(rawGyro.ZAxis));
                DEBUG_PORT.println("Sensors: MPU6050: Norm: " + String(normGyro.XAxis) + ", " + String(normGyro.YAxis) + ", " + String(normGyro.ZAxis));
              #endif
            #endif
          #endif
          #ifdef SENSOR_ENABLE_MPU6050_TEMP
            temperatureMPU6050 = mpu.readTemperature();
            #ifdef DEBUG_ENABLE_GLOBAL
              #ifdef DEBUG_ENABLE_MPU6050
                DEBUG_PORT.println("Sensors: MPU6050: Temp: " + String(temperatureMPU6050));
              #endif
            #endif
          #endif
        }
      }
      else {
        #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_MPU6050
            DEBUG_PORT.println("Sensors: MPU6050: Intialization Error!");     
          #endif
        #endif
      }
    }
  #endif 


  #ifdef SENSOR_ENABLE_BME280
    bool initializebme280(){   
      #ifdef DEBUG_ENABLE_GLOBAL
        #ifdef DEBUG_ENABLE_BME280s
          DEBUG_PORT.println("Sensors: BME280: Intitializing...");
        #endif
      #endif
      if (!bme.begin(0x76)) {
        #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_BME280
            DEBUG_PORT.println("Sensors: BME280: Intialization Failure!");
          #endif
        #endif
        return false;
      }
      #ifdef DEBUG_ENABLE_GLOBAL
        #ifdef DEBUG_ENABLE_BME280
          DEBUG_PORT.println("Sensors: BME280: Intialized...");
        #endif
      #endif
      return true;
    }

   void updateBME280Data() {
      if (millis() - previousUpdateMillisBME280 > UPDATE_INTERVAL_BME280) {
        #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_BME280
            DEBUG_PORT.println("Sensors: BME280: Updating BME280 data...");
          #endif
        #endif
        temperatureBME280 = bme.readTemperature();
        humidityBME280 = bme.readHumidity();
        pressureBME280 = bme.readPressure() / 100.0F;
        altitudeBME280 = bme.readAltitude(SEALEVELPRESSURE_HPA);
        #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_BME280
            DEBUG_PORT.println("Sensors: BME280: Temp: " + String(temperatureBME280));
            DEBUG_PORT.println("Sensors: BME280: Humidity: " + String(humidityBME280));
            DEBUG_PORT.println("Sensors: BME280: Pressure: " + String(pressureBME280));
            DEBUG_PORT.println("Sensors: BME280: Altitude: " + String(altitudeBME280));
          #endif
        #endif
        previousUpdateMillisBME280 = millis();
      } 
    }     
  #endif


 #ifdef SENSOR_ENABLE_DHT21
    bool initializeDHT21(){
      #ifdef DEBUG_ENABLE_GLOBAL
        #ifdef DEBUG_ENABLE_DHT21
          DEBUG_PORT.println("Baud rate set successfully...");
          dht.begin(); 
        #endif
      #endif    
  
    }

   void updateDHT21Data() {
     if (isnan(t) || isnan(h)){
        DEBUG_PORT.println("Failed to read from DHT");
       #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_BME280
            float h = dht.readHumidity();
            float t = dht.readTemperature();
            float H = bme.readHumidity();
            float T = bme.readTemperature();
            float P = bme.readPressure();         
          #endif
        #endif
      }
     else{
       #ifdef DEBUG_ENABLE_GLOBAL
          #ifdef DEBUG_ENABLE_MPU6050
            DEBUG_PORT.print("DHT:");
            DEBUG_PORT.print("Humidity: ");
            DEBUG_PORT.print(h);
            DEBUG_PORT.print(" %\t");  
            DEBUG_PORT.print("Temperature: ");               
            DEBUG_PORT.print(t);
            DEBUG_PORT.println(" *C");
            delay(2000);
         #endif
       #endif
     }
    } 
 #endif   
#endif

