#ifndef SENSORS_H_
#define SENSORS_H_

#include "Config.h"

#ifdef SENSOR_ENABLE_MPU6050
  bool initializeMPU6050();
  void updateMPU6050Data();
#endif

#ifdef SENSOR_ENABLE_BME280
  bool initializebme280();
  void updateBME280Data();
#endif

#ifdef SENSOR_ENABLE_DHT21
  bool initializeDHT21();
  void updateDHT21Data();
#endif

#endif



