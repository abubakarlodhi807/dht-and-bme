#ifndef DEBUG_H_
#define DEBUG_H_

#include "Config.h"
#include "Sensors.h"

void initializeDebugger();

void initializeDebugger() {
    #ifdef DEBUG_ENABLE_GLOBAL
      DEBUG_PORT.begin(DEBUG_BAUD);
      delay(4000);
      DEBUG_PORT.println("");
      DEBUG_PORT.println("\t\t###############################################");
      DEBUG_PORT.println("\t\tFirmware Name:     VSCode Snippet Stack");
      DEBUG_PORT.println("\t\tFirmware Version:  " + String(FW_VER));
      DEBUG_PORT.println("\t\tAuthor:            HyperNym IoT HW");
      DEBUG_PORT.println("\t\t###############################################");
      DEBUG_PORT.println("");
      delay(2000);
      DEBUG_PORT.println("Debugger: Successfully Initialized...");
    #endif
}

#endif