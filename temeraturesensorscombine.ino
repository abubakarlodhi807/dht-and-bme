#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <DHT.h>
#define DHTPIN 2    // modify to the pin we connected
#define BAUD_RATE   9600
#define DHTTYPE DHT21   // AM2301
DHT dht(DHTPIN, DHTTYPE);

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;

void setup() {
  Serial.begin(BAUD_RATE);
  
  Serial.println("Baud rate set successfully...");
 dht.begin();

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
}

void loop() {
  float h = dht.readHumidity();
 float t = dht.readTemperature();
 
 float H = bme.readHumidity();
 float T = bme.readTemperature();
 float P = bme.readPressure();

  
 // check if returns are valid, if they are NaN (not a number) then something went wrong!
 if (isnan(t) || isnan(h))
 {
   Serial.println("Failed to read from DHT");
 }
 else
 {
  Serial.print("DHT:");
   Serial.print("Humidity: ");
   Serial.print(h);
   Serial.print(" %\t");
   Serial.print("Temperature: ");
   Serial.print(t);
   Serial.println(" *C");
   delay(2000);
 }
 Serial.print("BME:");
  Serial.print("Temperature : ");
  Serial.print(T);
  Serial.print("*C");
  Serial.print(" \t");
  Serial.print("Pressure : ");
  Serial.print(P / 100.0F);
  Serial.print("hPa");
  Serial.print(" \t");
  Serial.print("Approx. Altitude = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.print("m");
  Serial.print(" %\t");
  Serial.print("Humidity : ");
  Serial.print(H);
  Serial.print(" %\t");
  Serial.println("");
  Serial.println("");
  delay(1000);
}
